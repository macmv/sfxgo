module gitlab.com/macmv/sfxgo

go 1.15

require (
	github.com/faiface/beep v1.0.2
	github.com/gdamore/tcell v1.4.0
)
