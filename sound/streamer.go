package sound

import (
  "math"

  "github.com/faiface/beep"
)

type streamer struct {
  time float64
  sr beep.SampleRate
  sample_length float64
  Settings []*Setting

  freq float64
  volume float64
}

func NewStream(sr beep.SampleRate) *streamer {
  return &streamer{
    time: 0,
    sr: sr,
    sample_length: 1.0 / float64(sr),
    Settings: new_settings(),
  }
}

func (s *streamer) Play() {
  s.freq = s.Settings[S_StartFreq].Get()
  s.volume = s.Settings[S_Volume].Get()
  s.time = 0
}

func (s *streamer) sample() float64 {
  s.freq += s.sample_length * s.Settings[S_Slide].Get()
  s.volume -= 1 / (s.Settings[S_DecayTime].Get() * float64(s.sr))
  if s.volume < 0 {
    s.volume = 0
  }

  return math.Sin(s.time * math.Pi * 2 * s.freq) * s.volume
}

func (s *streamer) Stream(samples [][2]float64) (int, bool) {
  for i := range samples {
    val := s.sample()

    samples[i][0] = val
    samples[i][1] = val

    s.time += s.sample_length
  }
  return len(samples), true
}

func (s *streamer) Err() error {
  return nil
}
