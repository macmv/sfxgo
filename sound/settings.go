package sound

const (
  S_Volume = iota

  S_AttackTime
  S_SustainTime
  S_SustainPunch
  S_DecayTime

  S_StartFreq
  S_MinFreq
  S_Slide
  S_VibratoDepth
  S_VibratoSpeed

  S_ChangeAmount
  S_ChangeSpeed

  S_SquareDuty
  S_DutySweep

  S_RepeatSpeed

  S_PhaserOffset
  S_PhaserSweep

  S_LpFilterCutoff
  S_LpFilterCutoffSweep
  S_LpFilterResonance
  S_HpFilterCutoff
  S_HpFilterCutoffSweep
)

func new_settings() []*Setting {
  return []*Setting{
    &Setting{name: "Volume"              , min: 0, max: 1.5},

    &Setting{name: "AttackTime"          , min: 0, max: 1},
    &Setting{name: "SustainTime"         , min: 0, max: 1},
    &Setting{name: "SustainPunch"        , min: 0, max: 1},
    &Setting{name: "DecayTime"           , min: 0, max: 1},

    &Setting{name: "StartFreq"           , min: 0, max: 10000},
    &Setting{name: "MinFreq"             , min: 0, max: 1},
    &Setting{name: "Slide"               , min: 0, max: 1},
    &Setting{name: "VibratoDepth"        , min: 0, max: 1},
    &Setting{name: "VibratoSpeed"        , min: 0, max: 1},

    &Setting{name: "ChangeAmount"        , min: 0, max: 1},
    &Setting{name: "ChangeSpeed"         , min: 0, max: 1},

    &Setting{name: "SquareDuty"          , min: 0, max: 1},
    &Setting{name: "DutySweep"           , min: 0, max: 1},

    &Setting{name: "RepeatSpeed"         , min: 0, max: 1},

    &Setting{name: "PhaserOffset"        , min: 0, max: 1},
    &Setting{name: "PhaserSweep"         , min: 0, max: 1},

    &Setting{name: "LpFilterCutoff"      , min: 0, max: 1},
    &Setting{name: "LpFilterCutoffSweep" , min: 0, max: 1},
    &Setting{name: "LpFilterResonance"   , min: 0, max: 1},
    &Setting{name: "HpFilterCutoff"      , min: 0, max: 1},
    &Setting{name: "HpFilterCutoffSweep" , min: 0, max: 1},
  }
}

type Setting struct {
  name string
  min, max, val float64
}

func (s *Setting) Set(val float64) {
  if val >= s.min && val <= s.max {
    s.val = val
  }
}

func (s *Setting) Name() string { return s.name }
func (s *Setting) Get() float64 { return s.val }
