package views

import (
  "github.com/gdamore/tcell"
)

type Screen struct {
  parent tcell.Screen
}

// This is a simpel wrapper aruond tcell.Screen, which just makes it implement
// the View interface.
func NewScreen(scr tcell.Screen) *Screen {
  return &Screen{
    parent: scr,
  }
}

// This calls screen.Show().
func (s *Screen) Draw() {
  s.parent.Show()
}

// This calls screen.SetContent().
func (s *Screen) Set(x, y int, val rune, style tcell.Style) {
  s.parent.SetContent(x, y, val, nil, style)
}

// This calls screen.Size().
func (s *Screen) Size() (int, int) {
  return s.parent.Size()
}

// This returns 0.
func (s *Screen) X() int { return 0 }
// This returns 0.
func (s *Screen) Y() int { return 0 }

// This does nothing.
func (s *Screen) SetPosition(int, int) { }
// This does nothing.
func (s *Screen) HandleMouse(e *tcell.EventMouse) {}
