package views

import (
  "github.com/gdamore/tcell"
)

type HBox struct {
  parent View
  children []View
  x, y int

  *Box
}

func NewHBox(parent View, x, y int, style tcell.Style) *HBox {
  return &HBox{
    parent: parent,
    Box: NewBox(parent, x, y, 0, 0, style),
  }
}

func (b *HBox) Draw() {
  for _, c := range b.children {
    c.Draw()
  }
  b.Box.Draw()
}

func (b *HBox) Add(child View) {
  w, h := child.Size()
  child.SetPosition(b.X() + b.Width() + 1, b.Y() + 1)
  bh := b.Height()
  if h < bh {
    h = bh
  }
  b.SetSize(w + b.Width(), h)
  b.children = append(b.children, child)
}

func (b *HBox) HandleMouse(e *tcell.EventMouse) {
  for _, c := range b.children {
    c.HandleMouse(e)
  }
}

func (b *HBox) SetPosition(x, y int) {
  b.Box.SetPosition(x, y)
  width := 0
  height := -1
  for _, c := range b.children {
    c.SetPosition(x + 1, y + 1)
    w, h := c.Size()
    x += w + 1
    width += w + 1
    if h > height || height == -1 {
      height = h
    }
  }
  b.SetSize(width, height)
}
