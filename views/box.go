package views

import (
  "github.com/gdamore/tcell"
)

type Box struct {
  parent View
  x, y, width, height int
  style tcell.Style
}

func NewBox(parent View, x, y, width, height int, style tcell.Style) *Box {
  pwidth, pheight := parent.Size()
  if width == -1 {
    width = pwidth
  }
  if height == -1 {
    height = pheight
  }
  return &Box{
    parent,
    x,
    y,
    width,
    height,
    style,
  }
}

func (b *Box) Draw() {
  b.parent.Set(b.x              , b.y               , '┌', b.style)
  b.parent.Set(b.x + b.width + 1, b.y               , '┐', b.style)
  b.parent.Set(b.x              , b.y + b.height + 1, '└', b.style)
  b.parent.Set(b.x + b.width + 1, b.y + b.height + 1, '┘', b.style)
  for i := 0; i < b.width; i++ {
    b.parent.Set(b.x + i + 1, b.y               , '─', b.style)
    b.parent.Set(b.x + i + 1, b.y + b.height + 1, '─', b.style)
  }
  for i := 0; i < b.height; i++ {
    b.parent.Set(b.x              , b.y + i + 1, '│', b.style)
    b.parent.Set(b.x + b.width + 1, b.y + i + 1, '│', b.style)
  }
}

func (b *Box) X() int { return b.x }
func (b *Box) Y() int { return b.y }
func (b *Box) SetPosition(x, y int) { b.x = x; b.y = y }

// This returns the width of the inside of the box.
func (b *Box) Width() int { return b.width }

// This returns the height of the inside of the box.
func (b *Box) Height() int { return b.height }

// This returns the total area covered by the box. This is width + 2 and height + 2.
func (b *Box) Size() (int, int) { return b.width + 2, b.height + 2 }

func (b *Box) Style() tcell.Style { return b.style }

func (b *Box) Inside(x, y int) bool {
  return x >= b.x && y >= b.y && x <= b.x + b.width + 1 && y <= b.y + b.height + 1
}

func (b *Box) SetSize(width, height int) {
  b.width = width
  b.height = height
}

func (s *Box) Set(x, y int, val rune, style tcell.Style) {}
func (s *Box) HandleMouse(e *tcell.EventMouse) {}
