package views

import (
  "github.com/gdamore/tcell"
)

type Label struct {
  parent View
  text string

  *Box
}

func NewLabel(parent View, x, y int, text string, style tcell.Style) *Label {
  return &Label{
    parent: parent,
    Box: NewBox(parent, x, y, len(text), 1, style),
    text: text,
  }
}

func (b *Label) Draw() {
  b.Box.Draw()
  for i := 0; i < len(b.text); i++ {
    // Using the comb[] can offset the reset of the line, and is unreliable
    b.parent.Set(
      b.X() + 1 + i,
      b.Y() + 1,
      rune(b.text[i]),
      b.Style(),
    )
  }
}

func (b *Label) SetText(text string) {
  b.text = text
  b.SetSize(len(text), 1)
}
