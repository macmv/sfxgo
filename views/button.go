package views

import (
  "github.com/gdamore/tcell"
)

type Button struct {
  parent View
  text string
  on_click func()

  *Box
}

func NewButton(parent View, x, y int, text string, style tcell.Style) *Button {
  return &Button{
    parent: parent,
    Box: NewBox(parent, x, y, len(text), 1, style),
    text: text,
  }
}

func (b *Button) Draw() {
  b.Box.Draw()
  for i := 0; i < len(b.text); i++ {
    // Using the comb[] can offset the reset of the line, and is unreliable
    b.parent.Set(
      b.X() + 1 + i,
      b.Y() + 1,
      rune(b.text[i]),
      b.Style(),
    )
  }
}

func (b *Button) HandleMouse(e *tcell.EventMouse) {
  x, y := e.Position()
  if e.Buttons() & tcell.Button1 != 0 && b.Inside(x, y) && b.on_click != nil {
    b.on_click()
  }
}

func (b *Button) OnClick(f func()) {
  b.on_click = f
}

func (b *Button) SetText(text string) {
  b.text = text
  b.SetSize(len(text), 1)
}
