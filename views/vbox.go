package views

import (
  "github.com/gdamore/tcell"
)

type VBox struct {
  parent View
  children []View
  x, y int

  *Box
}

func NewVBox(parent View, x, y int, style tcell.Style) *VBox {
  return &VBox{
    parent: parent,
    Box: NewBox(parent, x, y, 0, 0, style),
  }
}

func (v *VBox) Draw() {
  for _, c := range v.children {
    c.Draw()
  }
  v.Box.Draw()
}

func (v *VBox) Add(child View) {
  w, h := child.Size()
  child.SetPosition(v.X() + 1, v.Y() + v.Height() + 1)
  vw := v.Width()
  if w < vw {
    w = vw
  }
  v.SetSize(w, v.Height() + h)
  v.children = append(v.children, child)
}

func (v *VBox) HandleMouse(e *tcell.EventMouse) {
  for _, c := range v.children {
    c.HandleMouse(e)
  }
}

func (b *VBox) SetPosition(x, y int) {
  b.Box.SetPosition(x, y)
  width := -1
  height := 0
  for _, c := range b.children {
    c.SetPosition(x + 1, y + 1)
    w, h := c.Size()
    y += h
    height += h
    if w > width || width == -1 {
      width = w
    }
  }
  b.SetSize(width, height)
}
