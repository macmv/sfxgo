package views

import (
  "github.com/gdamore/tcell"
)

type Slider struct {
  parent View
  min, max, val float64
  on_change func(val float64)

  *Box
}

func NewSlider(parent View, x, y, width int, min, max float64, style tcell.Style) *Slider {
  return &Slider{
    Box: NewBox(parent, x, y, width, 1, style),
    parent: parent,
    min: min,
    max: max,
    val: min,
  }
}

func (s *Slider) Draw() {
  s.Box.Draw()
  pos := int((s.val - s.min) / (s.max - s.min) * float64(s.Box.Width())) + 1
  for i := 0; i < s.Box.Width(); i++ {
    // Using the comb[] can offset the reset of the line, and is unreliable
    let := ' '
    if i < pos {
      let = '-'
    }
    s.parent.Set(
      s.X() + 1 + i,
      s.Y() + 1,
      let,
      s.Style(),
    )
  }
}

func (s *Slider) HandleMouse(e *tcell.EventMouse) {
  x, y := e.Position()
  if e.Buttons() & tcell.Button1 != 0 && s.Inside(x, y) && s.on_change != nil {
    s.val = (float64(x - s.X() - 1) / float64(s.Width() - 1) * (s.max - s.min)) + s.min
    if s.val < s.min {
      s.val = s.min
    }
    if s.val > s.max {
      s.val = s.max
    }
    s.on_change(s.val)
  }
}

func (s *Slider) OnChange(f func(float64)) {
  s.on_change = f
}
