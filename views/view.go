package views

import (
  "github.com/gdamore/tcell"
)

type View interface {
  X() int
  Y() int
  SetPosition(int, int)
  Size() (int, int)
  Set(x, y int, val rune, style tcell.Style)
  HandleMouse(e *tcell.EventMouse)
  Draw()
}
