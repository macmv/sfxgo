package main

import (
  "time"

  "github.com/faiface/beep"
  "github.com/faiface/beep/speaker"

  "github.com/gdamore/tcell"

  "gitlab.com/macmv/sfxgo/views"
  "gitlab.com/macmv/sfxgo/sound"
)

func main() {
  scr, err := tcell.NewScreen()
  if err != nil { panic(err) }
  err = scr.Init()
  if err != nil { panic(err) }

  scr.EnableMouse()

  sr := beep.SampleRate(44100)
  speaker.Init(sr, sr.N(time.Second/10))
  stream := sound.NewStream(sr)
  speaker.Play(stream)

  s := views.NewScreen(scr)

  play := views.NewButton(s, 0, 0, "sup", tcell.StyleDefault)
  play.OnClick(func() {
    stream.Play()
  })

  sliders := views.NewVBox(s, 0, 3, tcell.StyleDefault)
  for _, setting := range stream.Settings {
    hbox := views.NewHBox(s, 0, 0, tcell.StyleDefault)

    label := views.NewLabel(s, 0, 0, setting.Name(), tcell.StyleDefault)
    hbox.Add(label)

    slider := views.NewSlider(s, 0, 3, 10, 5, 50, tcell.StyleDefault)
    slider.OnChange(func(v float64) {
      setting.Set(v)
    })
    hbox.Add(slider)
    sliders.Add(hbox)
  }

  // slider.OnChange(func(val float64) {
  //   play.SetText(fmt.Sprintln(val))
  // })

  for {
    e := scr.PollEvent()
    switch e := e.(type) {
    case *tcell.EventKey:
      if e.Key() == tcell.KeyEscape || e.Key() == 'q' {
        scr.Fini()
        return
      }
      scr.SetCell(0, 0, tcell.StyleDefault, rune(e.Key()))
    case *tcell.EventMouse:
      play.HandleMouse(e)
      sliders.HandleMouse(e)
    }
    sliders.Draw()
    play.Draw()
    scr.Show()
  }
}
